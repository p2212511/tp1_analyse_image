#include "RegionsGrowing.h"
#include <math.h>
#include <ctime>
#include <iostream>
#include<cmath>
#include "ManipulationImage.h"
#include <unordered_set>

RegionsGrowing::RegionsGrowing(const cv::Mat& image) :
	regions(cv::Mat::zeros(image.rows, image.cols, CV_8UC1)),
	nbGermes(0)
{
	image.convertTo(this->image, CV_8UC1);
	this->regions.convertTo(this->regions, CV_8UC1);
}

// **************************************
// Gestion des voisins d'un pixel
// **************************************
std::list<InfoPixel> RegionsGrowing::getVoisins(const InfoPixel& infoPixel)
{
	std::list<InfoPixel> voisins;
	ajouterVoisin(infoPixel.getX() - 1, infoPixel.getY(), voisins);
	ajouterVoisin(infoPixel.getX() + 1, infoPixel.getY(), voisins);
	ajouterVoisin(infoPixel.getX(), infoPixel.getY() - 1, voisins);
	ajouterVoisin(infoPixel.getX(), infoPixel.getY() + 1, voisins);

	return voisins;
}
bool RegionsGrowing::validerCoordonnees(int x, int y)const
{
	return (y >= 0 && y < image.cols) && (x >= 0 && x < image.rows);
}

void RegionsGrowing::ajouterVoisin(int x, int y, std::list<InfoPixel>& voisins)
{
	if (validerCoordonnees(x, y)) {
		voisins.push_back(InfoPixel(
			regions.at<uchar>(x, y),
			x,
			y,
			image.at<uchar>(x, y)
		));
	}
}
// **************************************
// Critere de croissance de regions
// **************************************
bool RegionsGrowing::critereCroissance(const InfoPixel& origin, const InfoPixel& voisin)
{
	return abs(origin.getValeur() - voisin.getValeur()) <=3.25;
}
// *******************************************************************************
// Critere de fusion de pixels de deux regions differentes sur la meme frontiere
// *******************************************************************************
bool RegionsGrowing::critereFusion(int origin, int voisin)
{
	return abs(origin - voisin) <= 10;
}
// **************************************
// Algorithme de region-growing
// **************************************

void RegionsGrowing::placerGermesAleatoire(int nbGermes)
{
	srand(time(0));
	this->colors = ManipulationImage::generer_couleurs_random(nbGermes);
	int x;
	int y;
	for (size_t i = 1; i <= nbGermes; i++)
	{
		do {
			x = rand() % image.rows;
			y = rand() % image.cols;
		} while (regions.at<uchar>(x, y) != 0);
		file.push(InfoPixel(i, x, y, image.at<uchar>(x, y)));
		regions.at<uchar>(x, y) = i;
	}
	this->nbGermes = nbGermes;
	std::cout << "Pose de germes\n";

}

void RegionsGrowing::placerGermesZoneRejetAleatoire(int nbGermes)
{
	srand(time(0));
	this->colors = ManipulationImage::generer_couleurs_random(nbGermes);
	int x;
	int y;

	/*
	rayon de la zone d'influence des germes:
	diagonale de l'image diviser par la racine carr�e nombre de germes que l'on souhaite utilis� pour recouvrir l'image,
	les germes restants seront repartis aleatoirement par l'algo.
	*/
	double seuilRejet = sqrt(pow(image.rows, 2) + pow(image.cols, 2)) / sqrt(nbGermes * 2 / 3);


	for (size_t i = 1; i <= nbGermes; i++)
	{
		bool rejet = false;
		do {
			x = rand() % image.rows;
			y = rand() % image.cols;

			// 5% de chance de passer outre la v�rification.
			if (rand() % 100 < 5) {
				// verification que le germes est en dehors de la zone d'influence d'un autre germes.
				for (InfoPixel p : file._Get_container()) {
					if (sqrt(pow(x - p.getX(), 2) + pow(x - p.getX(), 2)) <= seuilRejet)rejet = true;
				}
			}


		} while (regions.at<uchar>(x, y) != 0 && !rejet);
		file.push(InfoPixel(i, x, y, image.at<uchar>(x, y)));
		regions.at<uchar>(x, y) = i;
	}
	this->nbGermes = nbGermes;
}

void RegionsGrowing::croissanceRegions()
{
	std::cout << "Debut de croissance de regions \n";
	while (!file.empty()) {
		InfoPixel courrant = file.front();
		file.pop();
		std::list<InfoPixel> voisins = getVoisins(courrant);
		//std::cout << "Taille de la liste des voisins == " << voisins.size()<<"\n";
		std::cout << "";
		for (InfoPixel ip : voisins) {
			if (regions.at<uchar>(ip.getX(), ip.getY()) == 0 && critereCroissance(courrant, ip)) {
				regions.at<uchar>(ip.getX(), ip.getY()) = courrant.getClasse();
				ip.setClasse(courrant.getClasse());
				file.push(ip);
			}
		}
	}
	std::cout << "Fin de croissance de regions \n";
}

bool RegionsGrowing::fusionParContiguite()
{
	bool fusion_effectuee = false;
	cv::Mat effectifFrontiere = cv::Mat::zeros(this->nbGermes + 1, this->nbGermes + 1, CV_32S);
	cv::Mat similariteFrontiere = cv::Mat::zeros(this->nbGermes + 1, this->nbGermes + 1, CV_64F);
	cv::Mat similaritePoucentageFrontiere = cv::Mat::zeros(this->nbGermes + 1, this->nbGermes + 1, CV_64F);
	int r1 = 0;
	int r2 = 0;
	//calcul adjacence de region pixel par pixel horizontal
	for (size_t i = 0; i < image.rows; i++)
	{
		for (size_t j = 0; j < image.cols - 1; j++)
		{
			r1 = regions.at<uchar>(i, j);
			r2 = regions.at<uchar>(i, j + 1);
			if (r1 != r2) {
				effectifFrontiere.at<int>(r1, r2) += 1;
				effectifFrontiere.at<int>(r2, r1) += 1;
				if (critereFusion(image.at<uchar>(i, j), image.at<uchar>(i, j + 1))) {
					similariteFrontiere.at<double>(r1, r2) += 1;
					similariteFrontiere.at<double>(r2, r1) += 1;
				}
			}
		}

	}

	//calcul adjacence de region pixel par pixel vertical
	for (size_t i = 0; i < image.rows - 1; i++)
	{
		for (size_t j = 0; j < image.cols; j++)
		{
			r1 = regions.at<uchar>(i, j);
			r2 = regions.at<uchar>(i + 1, j);
			if (r1 != r2) {
				effectifFrontiere.at<int>(r1, r2) += 1;
				effectifFrontiere.at<int>(r2, r1) += 1;
				if (critereFusion(image.at<uchar>(i, j), image.at<uchar>(i + 1, j))) {
					similariteFrontiere.at<double>(r1, r2) += 1;
					similariteFrontiere.at<double>(r2, r1) += 1;
				}
			}
		}

	}

	for (size_t i = 0; i < effectifFrontiere.rows; i++)
	{
		for (size_t j = i; j < effectifFrontiere.cols; j++)
		{
			if (effectifFrontiere.at<int>(i, j) != 0) {
				similaritePoucentageFrontiere.at<double>(i, j) = similariteFrontiere.at<double>(i, j) / effectifFrontiere.at<int>(i, j);
			}
		}
	}
	/*
	cv::Mat colorRegions = cv::Mat::zeros(regions.rows, regions.cols, CV_8UC3);
	// Matrice des couleurs possible
	cv::Mat color = ManipulationImage::generer_couleurs_random(this->nbGermes + 1);
	std::cout << color;
	//coloriage
	for (size_t i = 0; i < regions.rows; i++)
	{
		for (size_t j = 0; j < regions.cols; j++)
		{
			int r = regions.at<uchar>(i, j);
			cv::Vec3b pixel = color.at<cv::Vec3b>(r, 0);
			colorRegions.at< cv::Vec3b>(i, j) = pixel;

		}
	}
	ManipulationImage::afficher_image(colorRegions, "colorRegionsBefore");
	*/
	std::vector<int> convertiseur(this->nbGermes + 1);
	for (size_t i = 0; i < convertiseur.size(); i++)
	{
		convertiseur[i] = i;
	}
	for (size_t i = 0; i < effectifFrontiere.rows; i++)
	{
		bool continuer = true;
		for (size_t j = 0; j < effectifFrontiere.cols && continuer; j++)
		{
			if (similaritePoucentageFrontiere.at<double>(i, j) >= 0.5) {
				convertiseur[j] = convertiseur[i];
				fusion_effectuee = true;
				continuer = false;
			}
		}
	}
	for (size_t i = 0; i < regions.rows; i++)
	{
		for (size_t j = 0; j < regions.cols; j++)
		{
			regions.at<uchar>(i, j) = convertiseur[regions.at<uchar>(i, j)];
		}

	}
	return fusion_effectuee;
}

void RegionsGrowing::fusionParSimilarite()
{
	std::vector<int> mesures;
	mesures.resize(this->nbGermes + 1);
	std::fill(mesures.begin(), mesures.end(), 0);
	std::vector<int> intensites;
	intensites.resize(this->nbGermes + 1);
	std::fill(intensites.begin(), intensites.end(), 0);
	std::vector<int> labels;
	labels.resize(this->nbGermes + 1);
	int cpt = 0;
	double intensite_moy = 0;
	double intensite = 0;
	std::cout << "Debut de fusion\n";
	// Calcul du niveau de gris moyen dans chaque region
	for (size_t i = 0; i < image.rows; i++)
	{
		for (size_t j = 0; j < image.cols; j++)
		{
			mesures[regions.at<uchar>(i, j)] += 1;
			intensites[regions.at<uchar>(i, j)] += image.at<uchar>(i, j);
		}
	}
	for (size_t i = 0; i < nbGermes + 1; i++)
	{
		if (mesures[i] != 0) {
			mesures[i] = (int)intensites[i] / mesures[i];
		}
	}

	for (size_t k = 1; k < mesures.size(); k++)
	{
		labels[k] = k;
	}
	// Utilisation de critere de similarite des regions : difference de niveau gris moyen <= 20
	for (size_t k = 1; k < mesures.size(); k++)
	{
		for (size_t l = k + 1; l < mesures.size(); l++)
		{
			int difference_intensite_moyenne = abs(mesures[k] - mesures[l]);
			if (difference_intensite_moyenne <= 19) {
				labels[l] = labels[k];
			}

		}
	}
	// Affectation des nouvelles regions
	for (size_t i = 0; i < regions.rows; i++)
	{
		for (size_t j = 0; j < regions.cols; j++)
		{
			regions.at<uchar>(i, j) = labels[regions.at<uchar>(i, j)];
		}

	}
	std::cout << "Fin de fusion\n";
}

cv::Mat RegionsGrowing::run()
{
	croissanceRegions();
	fusionParContiguite();
	fusionParSimilarite();
	return regions;
}
// **************************************
// Extraction de frontieres des regions
// **************************************

cv::Mat RegionsGrowing::extraireFrontiere(cv::Mat regions) {

	cv::Mat frontiere = cv::Mat::zeros(regions.rows, regions.cols, CV_8UC1);

	for (size_t i = 0; i < regions.rows; i++)
	{
		for (size_t j = 0; j < regions.cols; j++)
		{

			int rCase = regions.at<uchar>(i, j);
			if (i == 0 || j == 0 || i == regions.rows - 1 || j == regions.cols - 1)
				frontiere.at<uchar>(i, j) = rCase;
			else if (rCase != regions.at<uchar>(i - 1, j))
				frontiere.at<uchar>(i, j) = rCase;
			else if (rCase != regions.at<uchar>(i + 1, j))
				frontiere.at<uchar>(i, j) = rCase;
			else if (rCase != regions.at<uchar>(i, j - 1))
				frontiere.at<uchar>(i, j) = rCase;
			else if (rCase != regions.at<uchar>(i, j + 1))
				frontiere.at<uchar>(i, j) = rCase;
		}

	}
	return frontiere;
}

cv::Mat RegionsGrowing::colorierRegions(cv::Mat regions)
{
	// version color�e de regions
	cv::Mat colorRegions = cv::Mat::zeros(regions.rows, regions.cols, CV_8UC3);
	//coloriage
	for (size_t i = 0; i < regions.rows; i++)
	{
		for (size_t j = 0; j < regions.cols; j++)
		{
			int r = regions.at<uchar>(i, j);
			cv::Vec3b pixel = colors.at<cv::Vec3b>(r, 0);
			colorRegions.at< cv::Vec3b>(i, j) = pixel;

		}
	}
	return colorRegions;
}
void RegionsGrowing::placerGermesGrille(int nbLigne, int nbcolonnes, int nbGermeCase) {
	nbGermes = nbLigne * nbcolonnes * nbGermeCase;
	srand(time(0));
	this->colors = ManipulationImage::generer_couleurs_random(nbGermes);
	int colsize = image.cols / nbcolonnes;
	int rowsize = image.rows / nbLigne;
	for (size_t i = 0; i < nbLigne; i++)
	{
		for (size_t j = 0; j < nbcolonnes; j++)
		{

			for (size_t k = 0; k < nbGermeCase; k++)
			{
				int x;
				int y;
				do {
					y = (rand() % colsize) + colsize * j;
					x = (rand() % rowsize) + rowsize * i;
				} while (regions.at<uchar>(x, y) != 0);
				int r = i * nbcolonnes + j + 1;
				file.push(InfoPixel(r, x, y, image.at<uchar>(x, y)));
				regions.at<uchar>(x, y) = r;
			}
		}

	}
}
