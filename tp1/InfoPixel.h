#pragma once
#include "opencv2\imgproc.hpp"
#include <math.h>
#include "opencv2\highgui.hpp"
class InfoPixel
{
private:
	int classe;
	int x;
	int y;
	int valeur;
public:
	InfoPixel(int classe, int x, int y, int valeur);
	int getX()const;
	int getY()const;
	int getClasse()const;
	int getValeur()const;

	void setClasse(int classe);
	void afficher();
};

