#include "opencv2\highgui.hpp"
#include "opencv2\imgproc.hpp"
#include "ManipulationImage.h"
#include <iostream>
#include "RegionsGrowing.h"
using namespace std;
int main()
{
	// Lecture d'une image
	string path;
	cout << "Chemin vers l'image : ";
	cin >> path;
	cv::Mat image = ManipulationImage::lire_image(path, cv::IMREAD_GRAYSCALE);

	// Changement des dimensions d'une image
	// Commenter cette ligne si on veut garder les dimensions originales de l'image
	//image = ManipulationImage::changer_dimension(image, 512, 512);
	// L'egalisation est optionelle
	image = ManipulationImage::egalisation_histogramme(image);
	RegionsGrowing regionGrowing(image);

	// Pose de germes
	regionGrowing.placerGermesAleatoire(200);
	//regionGrowing.placerGermesGrille(5,5,4);
	//regionGrowing.placerGermesZoneRejetAleatoire(250);

	// Lancement du region-growing
	auto regions = regionGrowing.run();

	// Coloriage des regions resultat de la segmentation
	auto colorRegions = regionGrowing.colorierRegions(regions);
	
	// Extraction des frontieres des zones resultat de la segmentation
	auto frontieres = RegionsGrowing::extraireFrontiere(regions);
	
	// Coloriage des frontieres des regions resultat de la segmentation
	auto colorFrontieres = regionGrowing.colorierRegions(frontieres);
	
	// Affichage des resultats
	regions = regions * 40;
	ManipulationImage::afficher_image(image, "Image originale");
	ManipulationImage::afficher_image(regions, "Regions segementées gris");
	ManipulationImage::afficher_image(colorRegions, "Regions segementées coloriées");
	ManipulationImage::afficher_image(colorFrontieres, "Frontières Regions segementées");
	cv::waitKey(0);
	return 0;
}