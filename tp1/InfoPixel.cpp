#include "InfoPixel.h"
#include <iostream>
InfoPixel::InfoPixel(int classe, int x, int y, int valeur) :classe(classe), x(x), y(y), valeur(valeur)
{

}

int InfoPixel::getX()const
{
	return x;
}

int InfoPixel::getY()const
{
	return y;
}

int InfoPixel::getClasse()const
{
	return classe;
}

int InfoPixel::getValeur()const
{
	return valeur;
}

void InfoPixel::setClasse(int classe)
{
	this->classe = classe;
}

void InfoPixel::afficher()
{
	std::cout << "( classe == " << classe << " , x == " << x << " , y == " << y << " , valeur == " << valeur << " )" << std::endl;
}
