#pragma once
#include "opencv2\highgui.hpp"
#include "ManipulationImage.h"
#include <iostream>

using namespace std;

static class ManipulationImage
{
public:

	// Lire une image sous forme d'une matrice
	cv::Mat static lire_image(const string& path, cv::ImreadModes mode);

	// Changer les dimension d'une matrice image
	cv::Mat static changer_dimension(const cv::Mat& image, double longueur, double largeur);

	// Afficher une image
	void static afficher_image(const cv::Mat& image, const std::string& nom);

	// Calculer l'histogramme normalis� d'une matrice image
	cv::Mat static calculer_histogramme(const cv::Mat& image);

	// Normaliser histogramme
	cv::Mat static normaliser_histogramme(const cv::Mat& histogramme);

	// Calulculer l'histogramme cumul� d'une matrice image
	cv::Mat static calculer_histogramme_cumule(const cv::Mat& histogrammem);

	// Afficher un histogramme
	void static afficher_histogramme(const cv::Mat& histogramme, const  std::string& nom);

	// Etirer l'histogramme d'une matrice image
	// Le type de retour est l'image apr�s etirement
	cv::Mat static etirement_histogramme(const cv::Mat& image, int b, int a);

	// Egaliser l'histogramme d'une matrice image
	// Le type de retour est l'image apr�s egalisation
	cv::Mat static egalisation_histogramme(const cv::Mat& image);
	cv::Mat static generer_couleurs_random(int couleurs);
};

