#include "ManipulationImage.h"
#include "opencv2\imgproc.hpp"
#include <math.h>
cv::Mat ManipulationImage::lire_image(const string& path, cv::ImreadModes mode)
{
	string image_path;
	try
	{
		image_path = cv::samples::findFile(path);
	}
	catch (const exception& ex)
	{
		cout << ex.what();
		exit(-1);
	}

	cv::Mat image = imread(image_path, mode);
	if (image.empty())
	{
		throw std::invalid_argument("Le chemin saisie est invalide !");
		exit(-1);
	}
	return image;
}

cv::Mat ManipulationImage::changer_dimension(const cv::Mat& image, double longueur, double largeur)
{
	cv::Mat image_destination;
	cv::resize(image, image_destination, cv::Size(largeur, longueur));
	return image_destination;
}

void ManipulationImage::afficher_image(const cv::Mat& image, const std::string& nom)
{
	cv::imshow(nom, image);
}

cv::Mat ManipulationImage::calculer_histogramme(const cv::Mat& image)
{
	cv::Mat histogramme = cv::Mat::zeros(256, 1, CV_32S);
	double value = 0;
	for (size_t i = 0; i < image.rows; i++)
	{
		for (size_t j = 0; j < image.cols; j++)
		{
			value = (int)image.at<uchar>(i, j);
			histogramme.at<int>(value, 0) += 1;
		}
	}
	return histogramme;
}
cv::Mat ManipulationImage::normaliser_histogramme(const cv::Mat& histogramme) {
	cv::Mat histogramme_normalise = cv::Mat::zeros(256, 1, CV_32S);
	int max = histogramme.at<int>(0, 0);
	for (int i = 1; i < 256; i++) {
		if (max < histogramme.at<int>(i, 0)) {
			max = histogramme.at<int>(i, 0);
		}
	}
	for (int i = 0; i < 256; i++)
	{
		histogramme_normalise.at<int>(i, 0) = ((double)histogramme.at<int>(i, 0) / max) * 256;
	}
	return histogramme_normalise;
}
void ManipulationImage::afficher_histogramme(const cv::Mat& histogramme, const  std::string& nom)
{
	cv::Mat histogramme_normalise = normaliser_histogramme(histogramme);
	int hist[256];
	for (int i = 0; i < histogramme_normalise.rows; i++)
	{
		hist[i] = histogramme_normalise.at<int>(i, 0);
	}
	// Afficher histogramme
	int hist_w = 256; int hist_h = 400;
	int bin_w = cvRound((double)hist_w / 256);

	cv::Mat histImage(hist_h, hist_w, CV_8UC1, cv::Scalar(255, 255, 255));
	// draw the intensity line for histogramme
	for (int i = 0; i < 256; i++)
	{
		line(histImage, cv::Point(bin_w * (i), hist_h), cv::Point(bin_w * (i), hist_h - hist[i]), cv::Scalar(0, 0, 0), 1, 8, 0);
	}

	// display histogramme
	cv::namedWindow(nom, cv::WINDOW_NORMAL);
	cv::imshow(nom, histImage);
}

cv::Mat ManipulationImage::etirement_histogramme(const cv::Mat& image, int a, int b)
{
	double value = 0;
	double new_value = 0;
	cv::Mat new_image;
	cv::Mat histogramme = calculer_histogramme(image);
	int Nmin = 0;
	int Nmax = 255;
	for (size_t i = 0; i < histogramme.rows; i++)
	{
		if (histogramme.at<int>(i, 0) != 0) {
			Nmin = i;
			break;
		}
	}
	for (size_t i = histogramme.rows - 1; i >= 0; i--)
	{
		if (histogramme.at<int>(i, 0) != 0) {
			Nmax = i;
			break;
		}
	}
	image.convertTo(new_image, CV_8UC1);
	for (size_t i = 0; i < image.rows; i++)
	{
		for (size_t j = 0; j < image.cols; j++)
		{
			value = image.at<uchar>(i, j);
			new_value = (b - a) * ((value - Nmin) / (Nmax - Nmin)) + a;
			new_image.at<uchar>(i, j) = new_value;
		}
	}
	return new_image;
}

cv::Mat ManipulationImage::calculer_histogramme_cumule(const cv::Mat& histogramme)
{
	cv::Mat histogramme_cumule = cv::Mat::zeros(256, 1, CV_32S);
	histogramme_cumule.at<int>(0, 0) = histogramme.at<int>(0, 0);
	int value = 0;
	for (size_t i = 1; i < histogramme.rows; i++)
	{
		value = histogramme.at<int>(i, 0);
		histogramme_cumule.at<int>(i, 0) = histogramme_cumule.at<int>(i - 1, 0) + value;
	}
	return histogramme_cumule;
}

cv::Mat  ManipulationImage::egalisation_histogramme(const cv::Mat& image) {
	cv::Mat histogramme = calculer_histogramme(image);
	cv::Mat histogramme_cumule = calculer_histogramme_cumule(histogramme);
	cv::Mat new_image;
	image.convertTo(new_image, CV_8UC1);
	uchar value = 0, new_value = 0;
	double valeur_cumule;
	double _2D = pow(2, 8) - 1;
	for (size_t i = 0; i < image.rows; i++)
	{
		for (size_t j = 0; j < image.cols; j++)
		{
			value = image.at<uchar>(i, j);
			valeur_cumule = histogramme_cumule.at<int>(value, 0);
			new_value = _2D * (valeur_cumule / (image.rows * image.cols));
			new_image.at<uchar>(i, j) = new_value;
		}
	}
	return new_image;

}

cv::Mat ManipulationImage::generer_couleurs_random(int couleurs)
{
	cv::Mat color = cv::Mat::zeros(couleurs, 1, CV_8UC3);
	color.at<cv::Vec3b>(0, 0) = cv::Vec3b(0, 0, 0);
	srand(time(NULL));
	int r;
	int g;
	int b;
	for (size_t i = 1; i < couleurs; i++)
	{
		bool valide;
		do {
			valide = true;
			r = rand() % 255;
			g = rand() % 255;
			b = rand() % 255;
			for (size_t i = 0; i < i; i++)
			{
				cv::Vec3b c = color.at<cv::Vec3b>(i, 0);
				if (sqrt(pow(c[0] - r, 2) + pow(c[1] - g, 2)) + pow(c[2] - b, 2) <= 20) {
					valide = false;
				}
			}

		} while (!valide);

		color.at<cv::Vec3b>(i, 0) = cv::Vec3b(r, g, b);

	}
	return color;

}
