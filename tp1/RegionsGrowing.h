#pragma once
#include <queue>
#include "opencv2\imgproc.hpp"
#include "opencv2\highgui.hpp"
#include "InfoPixel.h"
#include "list"
class RegionsGrowing
{
private:
	cv::Mat image;
	std::queue<InfoPixel> file;
	cv::Mat regions;
	int nbGermes;
	cv::Mat colors;
	// Methodes
	void calculMinMax();
	std::list<InfoPixel> getVoisins(const InfoPixel& infoPixel);
	bool critereCroissance(const InfoPixel& origin, const InfoPixel& voisin);
	bool critereFusion(int origin, int voisin);
	bool validerCoordonnees(int x, int y) const;
	void ajouterVoisin(int x, int y, std::list<InfoPixel>& voisins);
	bool fusionParContiguite();
	void fusionParSimilarite();
	void croissanceRegions();
public:
	RegionsGrowing(const cv::Mat& image);
	cv::Mat run();
	void placerGermesGrille(int nbLigne, int nbcolonnes, int nbGermeCase);
	void placerGermesAleatoire(int nbGermes);
	void placerGermesZoneRejetAleatoire(int nbGermes);
	static cv::Mat extraireFrontiere(cv::Mat region);
	cv::Mat colorierRegions(cv::Mat regions);
};

